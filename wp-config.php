<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'mobile');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ys*/Ga,B^*I,Nd}[I&_KDwIvTmE6)z%kF.t~c3q_e|jZBI9TP6Fqn;.>O[E}V&Ct');
define('SECURE_AUTH_KEY',  'p76Gf&~8?TG?GKIj%^$[?6xhI9zgUGt)>_Q?L)CwBk[93Gf#ml5j^L8(+{U/uEMO');
define('LOGGED_IN_KEY',    'a+uRBMsr_W#Z*5bUM*U W$NL|sGMiGdweuT3{~$%c<CMiDSHA;u7Zft=P396(+FO');
define('NONCE_KEY',        '(ip7m&tR(P5IWWrtG$Lygy>:i~`xTInLst{7!([[%q@3_A)y2OTJ^+{d[Red,8v5');
define('AUTH_SALT',        'k}EBU(*DcFJM>p]b6Yz)d7]:z/Lj$ey)S>T!Q{92rCSrZ{5.4$>!. avKUa-S-]E');
define('SECURE_AUTH_SALT', '@BY/~=1dk@rZTZsl3R}pX5&`Svlx(.kAHrYZPbd5(v9@7D%_vm2YJLHRy8^+)g^#');
define('LOGGED_IN_SALT',   '#.DxSQQupBZs:6sB?NIrCI{+R:Rfg>j.&)PKWqDgkqT^jEqPYMLDwxz_9/2OXn3)');
define('NONCE_SALT',       '_AU~1lp~nc,?*|K:xS;y{n.3MQy/5P&rYGG+YQA}w?`WZ|>]euk$HQFm:t aei;B');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
