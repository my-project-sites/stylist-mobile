<?php 
    /* Template Name: Стилист */
	get_header(); 
	wp_reset_postdata();
?>

<div id="stylist">
    <h1 class="title title_page">Стилист</h1>
    <?php the_content();?>
</div>

<?php get_footer(); ?>