<?php 
    /* Template Name: Бровист */
	get_header(); 
	wp_reset_postdata();
?>

<div id="brow">
    <h1 class="title title_page">Бровист</h1>
    <?php the_content();?>
</div>

<?php get_footer(); ?>