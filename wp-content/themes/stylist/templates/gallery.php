<?php 
    /* Template Name: Галерея */
	get_header(); 
	wp_reset_postdata();
?>

<div id="content">
    <h1 class="title title_page"><?php the_title(); ?></h1>
    <div class="gallery">
        <img class="gallery__image img-thumbnail" src="<?=get_template_directory_uri();?>/assets/images/portfolio/1.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2" src="<?=get_template_directory_uri();?>/assets/images/portfolio/2.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2" src="<?=get_template_directory_uri();?>/assets/images/portfolio/3.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2" src="<?=get_template_directory_uri();?>/assets/images/portfolio/4.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2" src="<?=get_template_directory_uri();?>/assets/images/portfolio/5.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2" src="<?=get_template_directory_uri();?>/assets/images/portfolio/6.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2" src="<?=get_template_directory_uri();?>/assets/images/portfolio/7.jpg" alt="img">
        <img class="gallery__image img-thumbnail mt-2 mb-4" src="<?=get_template_directory_uri();?>/assets/images/portfolio/8.jpg" alt="img">
    </div>

    <a class="pagelink m-auto" href="https://instagram.com/kaza4enkova/" target="_blank">Смотреть все фото</a>
    
</div>

<?php get_footer(); ?>