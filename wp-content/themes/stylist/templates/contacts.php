<?php 
    /* Template Name: Контакты */
	get_header(); 
	wp_reset_postdata();
?>

<div id="contacts" class="contact-box">
    <h1 class="title title_page">Контакты</h1>

    <p class="text">Вы всегда можете задать любой интересующий вопрос и мы с радостью постараемся вам помочь.</p>
    
    <ul class="contact-box__list">
        <li class="contact-box__item">
            <a class="contact-box__link" href="mailto:<?=do_shortcode('[userEmail]');?>">
                <i class="fas fa-envelope"></i> <?=do_shortcode('[userEmail]');?>
            </a>
        </li>
        <li class="contact-box__item">
            <i class="fas fa-home"></i> <?=do_shortcode('[userAddress]');?>
        </li>
        <li class="contact-box__item">
            <i class="fas fa-phone-square"></i> <?=do_shortcode('[userPhone]');?>
        </li>
    </ul>
    
    <ul class="contact-box__network">
        <li class="list-inline-item">
            <a target="_blank" href="<?=do_shortcode('[userInstagram]');?>">
                <img class="contact-box__img" src="<?php echo get_template_directory_uri();?>/assets/images/instagram.png" alt="instagram">
            </a>
        </li>
        
        <li class="list-inline-item">
            <a href="<?=do_shortcode('[userTelegram]');?>">
                <img class="contact-box__img" src="<?php echo get_template_directory_uri();?>/assets/images/telegram.png" alt="telegram">
            </a>
        </li>
    </ul>
		          
    <div class="contact-box__map">
        <script charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Add97f15f29a0dac7007e81337d9c2c3ce9978847053dcc41aaba1ea9b1bd93f5&amp;width=100%&amp;height=220&amp;lang=ru_RU&amp;scroll=false"></script>
    </div>
    
</div>

<?php get_footer(); ?>