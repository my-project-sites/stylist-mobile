jQuery(document).ready(function($) {

    $('#menu').on('click', function() {
        $('.topmenu__list').toggleClass('topmenu__list_active');
    });

    $('.topmenu__link').on('click', function() {
        $('.topmenu__list').removeClass('topmenu__list_active');
    });

    $('#modal, #modal-2').on('click', function() {
        event.preventDefault();
        $('.modalbox').toggleClass('modal-active');
        $('body').css('overflow', 'hidden');
    });

    $('.modalbox__close').on('click', function() {
        $('.modalbox').removeClass('modal-active');
        $('body').css('overflow', 'auto');
    });

    // Cобытие клика по веб-документу
    $(document).mouseup(function (e) { 
        // Тут указываем ID элемента
        var div = $(".modalbox__content");
        // Если клик был не по нашему блоку
        if (!div.is(e.target)
            // И не по его дочерним элементам
		    && div.has(e.target).length === 0) {
            $('.modalbox').removeClass('modal-active');
            $('body').css('overflow', 'auto');
		}
    });

});