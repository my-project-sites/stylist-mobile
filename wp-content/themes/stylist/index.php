<?php 
	get_header(); 
	wp_reset_postdata();
?>

<header id="header">
    <div class="topmenu">
        <a href="/">
            <img class="topmenu__logo" src="<?=get_template_directory_uri();?>/assets/images/logo.png" alt="logo">
        </a>
        <ul class="topmenu__list">
            <li class="topmenu__item">
                <a class="topmenu__link" href="#services">Услуги</a>
            </li>
            <li class="topmenu__item">
                <a class="topmenu__link" href="#entry">Запись</a>
            </li>
            <li class="topmenu__item">
                <a class="topmenu__link" href="#gallery">Галерея</a>
            </li>
            <li class="topmenu__item">
                <a class="topmenu__link" href="#about">О себе</a>
            </li>
            <li class="topmenu__item">
                <a class="topmenu__link" href="#footer">Контакты</a>
            </li>
        </ul>

        <button id="menu" class="topmenu__button">
            <img src="<?=get_template_directory_uri();?>/assets/images/menu.png" alt="menu">
        </button>
    </div>
    <h1 class="title title_high">Наш салон красоты преобразит вас!</h1>
    <p class="text text_header">Наш салон — это штат профессиональных и квалифицированных специалистов, которые готовы решить любые ваши проблемы, связанные с кожей тела или лица. Мы гарантируем превосходный результат, который достигается благодаря колоссальному опыту наших мастеров. Наша цель — это найти наиболее оптимальное решение вашей проблемы, а не навязать свои услуги. Индивидуальный подход к каждому клиенту.</p>
</header>

<div id="services">
    <h2 class="title title_medium">Наш салон красоты предоставит вам отличные услуги по разумной цене. С нами вы всегда будете неотразимы</h2>

    <h3 class="title title_low title_service">Ознакомьтесь с нашими услугами</h3>

    <div class="services-box">
        <a class="pagelink" href="/brovist">Бровист</a>
        <a class="pagelink" href="/prajs">Прайс</a>
        <a class="pagelink" href="/stilist">Стилист</a>
    </div>
</div>

<div id="entry">
    <h2 class="title title_medium">Оставьте заявку на удобное для вас время. После чего с вами свяжутся и уточнят все детали</h2>
    <a id="modal" class="pagelink pagelink_entry" href="#">Записаться</a>
    <p class="text text_entry">Записывайтесь когда вам удобно - наши мастера имеют гибкий график работы!</p>
</div>

<div id="gallery">
    <h2 class="title title_medium">Мы собрали длля вас лучшие образцы наших работ</h2>
    <a class="pagelink pagelink_gallery" href="/galereya">Посмотреть</a>
    <p class="text text_gallery">Так же не забывайте подписаться на наш инстаграм 😉</p>
</div>

<div id="about">
    <div>
        <h2 class="title title_medium">Немного обо мне</h2>
        <p class="text text_about">Здесь я хочу поделиться с вами тем, что сподвигло меня встать на тот путь по которому иду.</p>
        <a class="pagelink pagelink_about" href="/lichnaya-istoriya">Прочитать</a>
    </div>

    <div class="about_boottom">
        <h2 class="title title_medium">Контактная информация</h2>
        <p class="text text_about">Если вы не знаете как к нам попасть, то ознакомтесь, пожалуйста, с нашими контактными данными. Также подпишитесь на наши соцсети.</p>
        <a class="pagelink pagelink_about" href="/kontakty">Контакты</a>
    </div>
</div>

<?php get_footer(); ?>