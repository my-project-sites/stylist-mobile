<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title></title>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <?php wp_head(); ?>

    <link rel="icon" href="<?=get_template_directory_uri();?>/assets/images/favicon.ico" type="image/x-icon">

</head>
<body>