<footer id="footer">
    <a href="#header" class="link_scroll">Вверх</a>

    <div class="brandbox">
        <div class="brandbox__box">
            <img src="<?=get_template_directory_uri();?>/assets/images/bourjois.png" alt="bourjois">
        </div>

        <div class="brandbox__box">
            <img src="<?=get_template_directory_uri();?>/assets/images/schwarzkopf.png" alt="schwarzkopf">
        </div>

        <div class="brandbox__box">
            <img src="<?=get_template_directory_uri();?>/assets/images/loreal.png" alt="loreal">
        </div>

        <div class="brandbox__box">
            <img src="<?=get_template_directory_uri();?>/assets/images/avon.png" alt="avon">
        </div>
    </div>

    <ul class="footer-menu">
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="/">Главная</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="/prajs">Прайс</a>
        </li>
        <li class="footer-menu__item">
            <a id="modal-2" class="footer-menu__link" href="#">Запись</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="/galereya">Галерея</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="/lichnaya-istoriya">О себе</a>
        </li>
        <li class="footer-menu__item">
            <a class="footer-menu__link" href="/kontakty">Контакты</a>
        </li>
    </ul>

    <hr class="separator">

    <ul class="footer-submenu">
        <li class="footer-submenu__item">
            <a class="footer-submenu__link" href="/stilist">Стилист</a>
        </li>
        <li class="footer-submenu__item">
            <a class="footer-submenu__link" href="/brovist">Бровист</a>
        </li>
        <li class="footer-submenu__item">
            <a target="_blank" class="footer-submenu__link" href="#">Инстаграм</a>
        </li>
    </ul>

    <h3 class="title title_low title_footer">DESIGN AND DEVELOP</h3>

    <div class="develop">
        <a target="_blank" href="https://www.facebook.com/bloodborne.gothic/">
            <img class="develop__logo" src="<?=get_template_directory_uri();?>/assets/images/my-logo.png" alt="logo">
        </a>
    </div>

    <p class="copyright">ALL RIGHTS RESERVED &#169;</p>
</footer>

<?php wp_footer(); ?>

<!-- Modal -->
<div class="modalbox">
    <div class="modalbox__content">
        <?php echo do_shortcode('[contact-form-7 id="8"]'); ?>
        <button class="modalbox__close">Закрыть</button>
    </div>
</div>

<script>
// Переадресация на основной сайт
if(document.documentElement.clientWidth > 1301) {
    location.href = 'http://stylist.host';
}
</script>

</body>
</html>