<?php 
	get_header();
	wp_reset_postdata(); 
?>

<div id="insta">
    <h1 class="title title_page"><?php the_title(); ?></h1>
    <?php the_content(); ?>
</div>

<?php get_footer(); ?>