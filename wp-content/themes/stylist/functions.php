<?php

# Шорткод для home_url
add_shortcode('main_url', 'url_func');
function url_func() {
    return get_home_url();
}

# Шорткод для site_name
add_shortcode('site_name', 'site_name');
function site_name() {
    $name = get_home_url();
    $number = stripos($name, '//');
    return 'www.' . substr($name, $number + 2);
}

# Отладочная функция
function out($array) {
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}

# Подключаем стили в шапке
add_action('wp_enqueue_scripts', 'header_styles');
function header_styles() {
	wp_enqueue_style('Fira+Sans', get_template_directory_uri() . '/assets/fonts/firasans/firasans.css');
	wp_enqueue_style('Arimo', get_template_directory_uri() . '/assets/fonts/arimo/arimo.css');
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css');
	wp_enqueue_style('main', get_template_directory_uri() . '/assets/css/main.css');
}

# Подключаем стили в футере
add_action('wp_footer', 'footer_styles');
function footer_styles() {
	wp_enqueue_style('style', get_stylesheet_uri());
}

# Подключаем скрипты в шапке
add_action('wp_enqueue_scripts', 'header_scripts');
function header_scripts() {}

# Подключаем скрипты в шапке
add_action('wp_footer', 'footer_scripts');
function footer_scripts() {
	wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js');
	# Home
	if(is_home()) {
		wp_enqueue_script('home', get_template_directory_uri() . '/assets/js/home.js');
	}
}

# Отключаем Гутенберг
if('disable_gutenberg') {
	add_filter('use_block_editor_for_post_type', '__return_false', 100);
	remove_action('wp_enqueue_scripts', 'wp_common_block_scripts_and_styles');
	add_action('admin_init', function(){
		remove_action( 'admin_notices', ['WP_Privacy_Policy_Content', 'notice'] );
		add_action( 'edit_form_after_title', ['WP_Privacy_Policy_Content', 'notice']);
	});
}

# Удалить атрибут type у styles
add_filter('style_loader_tag', 'clean_style_tag');
function clean_style_tag($src) {
    return str_replace("type='text/css'", '', $src);
}

# Удалить атрибут type у scripts 
add_filter('script_loader_tag', 'clean_script_tag');
function clean_script_tag($src) {
    return str_replace("type='text/javascript'", '', $src);
}

# Асинхронная загрузка для скриптов, подключенных через wp_enqueue_script
add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
function add_async_attribute($tag, $handle) {
	if(!is_admin()) {
	    if ('jquery-core' == $handle) {
	        return $tag;
	    }
	    return str_replace(' src', ' defer src', $tag);
	}
	else {
		return $tag;
	}
}

# Подключение стилей в админке
add_action('admin_head', 'wph_inline_css_admin');

function wph_inline_css_admin() {
echo 

'<style>
	.plugin-title span a[href="http://www.never5.com/?utm_source=plugin&utm_medium=link&utm_campaign=what-the-file"] {
		display: none !important;
	}

	#adminmenuback, #adminmenuwrap, #adminmenu {
		background-color: #23282d !important;
	}

	.wramvp_img_email_image,
	#wp-admin-bar-view-store, 
	#wp-admin-bar-view-site,
	.aioseop_help_text_link, 
	.upgrade_menu_link, 
	#aio-pro-update, 
	.proupgrade, .notice-error, .column-RV,
	.column-MV, .woocommerce-help-tip,
	.updated, #dashboard_php_nag,
	.mailpoet-dismissible-notice,
	.skiptranslate,
	.mailpoet_feature_announcement,
	.elementor-plugins-gopro,
	.aioseop-notice-woocommerce_detected,
	.aioseop_nopad_all,
	.help_tip, .aio_orange_box,
	.notice-warning {
		display: none !important;
	}

	#thumb {
		color: transparent;
	}
</style>';

}

?>